//import 'dart:html';

import 'package:flutter/material.dart';
import 'departamentos/departaments.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  static String title;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHome(),
    );
  }
}

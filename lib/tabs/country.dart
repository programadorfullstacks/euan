import 'package:flutter/material.dart';

import 'package:url_launcher/url_launcher.dart';
import 'package:getwidget/getwidget.dart';

void main() => runApp(MaterialApp(
      home: (Countrytabs()),
    ));

class Countrytabs extends StatefulWidget {
  @override
  _CountrytabsState createState() => _CountrytabsState();
}

class _CountrytabsState extends State<Countrytabs> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.green[100],

      body: Container(
        child: ListView(
          children: [
            new Padding(padding: new EdgeInsets.all(10.00)),
            ListTile(
              title: Text(
                "Mensaje",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 23, color: Colors.black),
              ),
            ),
            ListTile(
              subtitle: RichText(
                textAlign: TextAlign.justify,
                text: TextSpan(
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontStyle: FontStyle.italic),
                    children: [
                      TextSpan(
                          text:
                              '"En estos tiempos dificiles, la tecnologia juega un papel tan importante que incluso es la unica manera para que el mundo no se detenga, la educacion no es una excepción. Es por eso que la tematica de esta App o más bien el objetivo, es el dar a conocer el Tecnologico De Chiná pero de una forma distinta en la cual los aspirantes y sociedad en general puedan dar un vistazo a las bastas areas que el instituto ofrece, todo esto pensando en las muchas dificultades de movilización que existen en nuestro país"'),
                    ]),
              ),
            ),
            new Padding(padding: new EdgeInsets.all(10.00)),
            Column(
              children: [
                ListTile(
                  title: Text(
                    'Medios de contacto',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 23, color: Colors.black),
                  ),
                ),
                GestureDetector(
                  child: GFListTile(
                    avatar: GFAvatar(
                      radius: 20,
                      backgroundImage: NetworkImage(
                          "https://cdn.pixabay.com/photo/2013/07/13/14/05/location-162102_960_720.png"),
                    ),
                    onTap: _launchUL,
                    titleText: 'DIRECCIÓN',
                    subTitle: Text(
                        'Calle 11 S/N ENTRE 22 Y 28, CHINÁ, CAM. MÉXICO. C.P. 24520'),
                  ),
                ),
                GFListTile(
                  avatar: GFAvatar(
                    radius: 20,
                    backgroundImage: NetworkImage(
                        "https://cdn.pixabay.com/photo/2015/12/08/18/39/at-sign-1083508__340.png"),
                  ),
                  titleText: 'CORREO ELECTRONICO',
                  subTitle: Text('dir01_china@tecnm.mx'),
                ),
                GFListTile(
                  avatar: GFAvatar(
                    radius: 20,
                    backgroundImage: NetworkImage(
                        "https://cdn.pixabay.com/photo/2016/06/06/16/39/phone-1439839__340.png"),
                  ),
                  titleText: 'NUMEROS TELEFONICOS',
                  subTitle: Text('(981) 82 7 20 81'),
                ),
                ListTile(
                  title: Text(
                    'Visita nuestras redes sociales',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 23, color: Colors.black),
                  ),
                ),
                GestureDetector(
                  child: GFListTile(
                    avatar: GFAvatar(
                      radius: 20,
                      backgroundImage: NetworkImage(
                          "https://cdn.pixabay.com/photo/2017/06/22/06/22/facebook-2429746_960_720.png"),
                    ),
                    onTap: _launchURL,
                    titleText: 'FACEBOOK 1',
                    subTitle: Text(''),
                  ),
                ),
                GestureDetector(
                  child: GFListTile(
                    avatar: GFAvatar(
                      radius: 20,
                      backgroundImage: NetworkImage(
                          "https://cdn.pixabay.com/photo/2017/06/22/06/22/facebook-2429746_960_720.png"),
                    ),
                    onTap: _launchUR,
                    titleText: 'FACEBOOK 2 ',
                    subTitle: Text(''),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

_launchUL() async {
  const url =
      'https://www.google.com.mx/maps/@19.7711313,-90.5013633,3a,75y,239.54h,76.5t/data=!3m7!1e1!3m5!1sHdb815jic4zRDQcxvJOA8Q!2e0!6shttps:%2F%2Fstreetviewpixels-pa.googleapis.com%2Fv1%2Fthumbnail%3Fpanoid%3DHdb815jic4zRDQcxvJOA8Q%26cb_client%3Dmaps_sv.tactile.gps%26w%3D203%26h%3D100%26yaw%3D295.12933%26pitch%3D0%26thumbfov%3D100!7i13312!8i6656?hl=es&authuser=0';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

_launchURL() async {
  const url = 'https://www.facebook.com/tecnmcampus.china';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

_launchUR() async {
  const url = 'https://www.facebook.com/DAE.TECNMCHINA';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

/*Widget nombre() {
  return Text("ESTUDIATES GRADUADOS EN TECNM",
      textAlign: TextAlign.center,
      style: TextStyle(
          color: Colors.black, fontSize: 20.0, fontWeight: FontWeight.bold));
}*/
import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_svg/svg.dart';
/*
import 'munulateral/organigrama.dart';
import 'munulateral/mensajedeldirector.dart';
import 'munulateral/historia.dart';
import 'munulateral/directoriomaestros.dart';
import 'munulateral/normateca.dart';
*/
import 'package:getwidget/getwidget.dart';

import 'munulateral/totalmensaje.dart';

//import 'package:playmedia/personal.dart';

void main() => runApp(FirstTab());

// ignore: must_be_immutable
class FirstTab extends StatelessWidget {
  List<String> images = [
    "https://drive.google.com/uc?export=view&id=1Hn2zgosPTFcac-ybPIw5uNm1-6oMU2fQ",
    "https://drive.google.com/uc?export=view&id=16A2IXnzgWL1B8ce6hjyQO-NgdSem3P_m",
    "https://drive.google.com/uc?export=view&id=1ODpCwzeLW0UVRqqkzbai6YeHIX7WMT_E"
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.blue[900],
            elevation: 0,
            title: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // ignore: missing_required_param
                IconButton(
                  icon: Image.network(
                      "https://drive.google.com/uc?export=view&id=1egwV-HxzGBCXR3j97iBMOtP8ikPheXpF",
                      width: 100),
                ),
                Text(
                  'TECNM CAMPUS CHINÁ',
                  style: TextStyle(fontSize: 15, color: Colors.white),
                ),
              ],
            ),
          ),
          body: Container(
              margin: EdgeInsets.all(5.0),
              child: ListView(
                children: [
                  _swiper(),
                  SizedBox(
                    height: 20,
                  ),
                  GFListTile(
                    titleText: 'MISIÓN',
                    subTitle: Text(
                      'Formar profesionales humanistas con pertinencia, portadores de conocimientos vanguardistas y competitivos; emprendedores e innovadores a través de una educación superior científica y tecnológica de calidad.',
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  GFListTile(
                    titleText: 'VISIÓN',
                    subTitle: Text(
                      'Ser una Institución educativa formadora de ciudadanos del mundo a través del desarrollo sostenido sustentable y equitativo. Con personal capacitado con altos estándares de calidad; protectora del medio ambiente e interactuando con las necesidades que exigen los cambios del País y la comunidad.',
                      textAlign: TextAlign.justify,
                    ),
                  ),
                  new Padding(padding: new EdgeInsets.all(4.00)),
                  GestureDetector(
                    child: Card(
                      child: Column(
                        children: <Widget>[
                          ListTile(
                            title: const Text('MENSAJE DEL DIRECTOR'),
                            leading: Image.network(
                              "https://scontent.fcjs3-1.fna.fbcdn.net/v/t1.6435-9/84018973_3495760897160571_3499340680478588928_n.jpg?_nc_cat=107&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeEv9mlBx-gudDKsSd4kh381ampbY2ko2DtqaltjaSjYOx6z1kOihe9tFEJuuyLGa7wXpFByW-E--ZpbkHmxRAwX&_nc_ohc=N8YJ0eEp6EoAX8tAigX&_nc_pt=1&_nc_ht=scontent.fcjs3-1.fna&oh=5b27d64e4d2de1dcc844f3bb8298020d&oe=60F31685",
                              width: 90,
                              height: 500,
                            ),
                            minLeadingWidth: 20,
                            subtitle: Text(
                              'El Instituto Tecnológico de Chiná (ITChiná), a 45 años de su creación, ha formado a más de 2,680 profesionistas en las diferentes carreras que ha ofertado. ',
                              textAlign: TextAlign.justify,
                            ),
                            minVerticalPadding: 1,
                          ),

                          // Usamos una fila para ordenar los botones del card
                        ],
                      ),
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Pagina()),
                      );
                    },
                  ),
                  Card(
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('HISTORIA'),
                          leading: Image.network(
                            "https://scontent.fcjs3-1.fna.fbcdn.net/v/t1.6435-9/70934289_3080545605348771_726475266938896384_n.jpg?_nc_cat=110&ccb=1-3&_nc_sid=730e14&_nc_eui2=AeHQFKa4zrzc8lHW509ErX9F7V4Yb2Zn9JvtXhhvZmf0m47RIgjyGXzSAvd3AKmL-tZuA4xQnA37hn3YvZmvbQST&_nc_ohc=2oZfD35i2fwAX8vTkdo&_nc_pt=1&_nc_ht=scontent.fcjs3-1.fna&oh=0670d5948b0cf4e31e0dfe5248c02e25&oe=60F4A350",
                            width: 90,
                            height: 500,
                          ),
                          minLeadingWidth: 20,
                          subtitle: Text(
                            'Éste fue el primer tecnológico creado en el Estado de Campeche, el 1 de septiembre de 1975 abrió sus puertas a la juventud estudiosa con vocación agropecuaria.',
                            textAlign: TextAlign.justify,
                          ),
                          minVerticalPadding: 1,
                        ),
                      ],
                    ),
                  ),
                  Card(
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('DIRECTORIO'),
                          leading: Image.network(
                            "https://c.pxhere.com/photos/ed/83/book_pen_writing_paper_table-101800.jpg!d",
                            width: 90,
                            height: 500,
                          ),
                          minLeadingWidth: 20,
                          subtitle: Text(
                            '',
                            textAlign: TextAlign.justify,
                          ),
                          minVerticalPadding: 1,
                        ),

                        // Usamos una fila para ordenar los botones del card
                      ],
                    ),
                  ),
                  Card(
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('ORGANIGRAMA'),
                          leading: Image.network(
                            "https://c.pxhere.com/photos/cc/97/mark_marker_hand_leave_production_planning_control_organizational_structure_work_process-774947.jpg!d",
                            width: 90,
                            height: 500,
                          ),
                          minLeadingWidth: 20,
                          subtitle: Text(
                            '',
                            textAlign: TextAlign.justify,
                          ),
                          minVerticalPadding: 1,
                        ),

                        // Usamos una fila para ordenar los botones del card
                      ],
                    ),
                  ),
                  Card(
                    child: Column(
                      children: <Widget>[
                        ListTile(
                          title: const Text('NORMATECA'),
                          leading: Image.network(
                            "https://c.pxhere.com/photos/b0/bc/hammer_books_law_court_lawyer_paragraphs_rule_jura-738484.jpg!s",
                            width: 90,
                            height: 500,
                          ),
                          minLeadingWidth: 20,
                          subtitle: Text(
                            '',
                            textAlign: TextAlign.justify,
                          ),
                          minVerticalPadding: 1,
                        ),

                        // Usamos una fila para ordenar los botones del card
                      ],
                    ),
                  ),
                ],
              ))),
    );
  }

// este pedaso de codigo es del carrucel
  Widget _swiper() {
    return Container(
      width: double.infinity,
      height: 180.0,
      child: Swiper(
        viewportFraction: 0.8,
        scale: 0.9,
        itemBuilder: (BuildContext context, int index) {
          return new Image.network(
            images[index],
            fit: BoxFit.fill,
          );
        },
        itemCount: 3,
        control: new SwiperControl(color: Colors.blue[900]),
      ),
    );
  }

// esta parte de codigo son de los iconos de mensaje  historia etc//
  SizedBox regularCard(String iconName, String cardLabel) {
    return SizedBox(
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.blue[900],
          ),
          child: SvgPicture.asset(iconName,
              width:
                  40), //se utiliza puras imagenes svg y que esten guardatos en la carpeta no he buscado como traerlo de la web
        ),
        SizedBox(height: 5),
        Text(cardLabel,
            textAlign: TextAlign.center,
            style: textStyle(13, FontWeight.w500, Colors.blue))
      ]),
    );
  }

  Container mainCard(context) {
    return Container(child: Row(children: []));
  }

  TextStyle textStyle(double size, FontWeight fontWeight, Color colorName) =>
      TextStyle(
        color: colorName,
        fontSize: size,
        fontWeight: fontWeight,
      );
}

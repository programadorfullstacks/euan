import 'package:flutter/material.dart';
// ignore: unused_import
import 'package:playmedia/tabs/munulateral/totalmensaje.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('MENSAJE DEL DIRECTOR'),
      ),
      body: ListView(
        padding: EdgeInsets.all(8),
        children: <Widget>[
          Card(
            child: Column(
              children: <Widget>[
                ListTile(
                  title: const Text('MENSAJE DEL DIRECTOR'),
                  leading: Image.network(
                    "https://th.bing.com/th/id/R.a5a64a473eee1d51fa04c41aac9d26a0?rik=5qCsgEGR21G5FQ&pid=ImgRaw",
                    width: 90,
                    height: 500,
                  ),
                  minLeadingWidth: 20,
                  subtitle: Text(
                    'El Instituto Tecnológico de Chiná (ITChiná), a 45 años de su creación, ha formado a más de 2,680 profesionistas en las diferentes carreras que ha ofertado. ',
                    textAlign: TextAlign.justify,
                  ),
                  minVerticalPadding: 1,
                ),

                // Usamos una fila para ordenar los botones del card
              ],
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

class Historia1 extends StatelessWidget {
  const Historia1({key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("HISTORIA COMPLETO"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: ListView(
          children: [
            Text(
              "El ITChiná inició a partir de abril de 2014 un proceso profundo de transformación, consistente en trabajar sobre el cumplimiento de indicadores de calidad. Para ello; se trabajó en una autoevaluación con fines de acreditación de todos sus programas académicos acreditables, logrando a la fecha la acreditación de todos:",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Ingeniería en Agronomía: Acreditada por el Comité Mexicano de Acreditación de la Educación Agronómica A.C. (COMEAA) – COPAES para el periodo noviembre de 2014 a noviembre de 2019.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Ingeniería Forestal: Acreditada por el Comité Mexicano de Acreditación de la Educación Agronómica A.C. (COMEAA) – COPAES para el periodo abril de 2015 a abril de 2020.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Licenciatura en Biología: acreditada por el Comité de Acreditación y Certificación de la Licenciatura en Biología A.C. (CACEB) – COPAES para el periodo diciembre de 2014 a diciembre de 2019.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Ingeniería en Gestión Empresarial: Acreditada por el Consejo de Acreditación en Ciencias Administrativas Contables y Afines A.C. (CACECA) – COPAES para el periodo abril de 2015 a abril de 2020.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Ingeniería en Administración: Acreditada por el Consejo de Acreditación en Ciencias Administrativas Contables y Afines A.C. (CACECA) – COPAES para el periodo mayo de 2016 a mayo de 2021.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Logrando con lo anterior la distinción por parte de la Secretaría de Educación Pública de: “INSTITUCIÓN DE EXCELENCIA ACADÉMICA”.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Para atender los servicios ofertados, el ITChiná cuenta con los siguientes recursos humanos: 66 profesores y 27 de apoyo y asistencia a la educación. Del total de profesores, 9 cuentan con doctorado y 36 con maestría; de los cuales 7 están en el Sistema Nacional de Investigadores (SNI) y 10 cuentan con el reconocimiento de Perfil Deseable del PROMEP. Actualmente se encuentran operando 3 cuerpos académicos: Investigación Agropecuaria y Forestal para el Desarrollo Sustentable del Trópico, Agroecología y Desarrollo Sustentable y Agroescosistemas y Conservación de la Biodiversidad.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Como parte del proceso de mejora, se ha fortalecido la imagen e infraestructura del plantel; consistente en dotar a todas las aulas de internet, A/A y TICs. Se ha modernizado el Centro de Información, automatizando el servicio, ofreciendo el servicio de estantería abierta y la suscripción a dos bibliotecas virtuales. Se ha iniciado la adecuación, mantenimiento y equipamiento de laboratorios, talleres y áreas de producción e investigación, con el propósito de brindar un mejor servicio a estudiantes y productores, además de fortalecer la investigación.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Se ha ampliado y mejorado la infraestructura deportiva, con la construcción de la cancha de futbol y beisbol con pasto natural y sistema de riego y un polifuncional techado, con gradas; para realizar actividades culturales, cívicas y deportivas, fortaleciendo la formación integral de nuestros alumnos.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Se está fortaleciendo la Unidad Experimental y de Investigación “Xamantún”, que tiene una superficie de 150 has, destinadas a plantaciones de diversas especies, convenios de colaboración y vinculación ejecutándose in situ, proyectos de investigación y áreas productivas (tanto agrícolas como pecuarias). En este sentido, se cuenta con un hato bovino de registro; también en Xamantún, se tienen: porcinos, ovinos y caprinos, para desarrollar diversas actividades académicas, de investigación y de producción en el ámbito pecuario. Dicha unidad está dotada con múltiples implementos y maquinaria agrícola, infraestructura e instalaciones necesarias para el desarrollo de las actividades en campo, todo lo anterior para apuntalar y reforzar las competencias de los estudiantes que cursan los diversos programas educativos que ofertamos; así como para fortalecer las actividades de docencia, investigación y producción del plantel.",
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 15),
            Text(
              "Con esto reiteramos nuestro compromiso con nuestros estudiantes y con la sociedad de: “APRENDER PRODUCIENDO”.",
              textAlign: TextAlign.justify,
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

import 'historiacompleto.dart';

class MyApp1 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('MENSAJE DEL DIRECTOR'),
      ),
      body: ListView(
        padding: EdgeInsets.all(8),
        children: <Widget>[
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                Image.network(
                    "https://th.bing.com/th/id/R.a5a64a473eee1d51fa04c41aac9d26a0?rik=5qCsgEGR21G5FQ&pid=ImgRaw"),
                SizedBox(height: 20),
                ListTile(
                  title: const Text('MENSAJE DEL DIRECTOR'),
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Text(
                    'El Instituto Tecnológico de Chiná (ITChiná), a 45 años de su creación, ha formado a más de 2,680 profesionistas en las diferentes carreras que ha ofertado. Actualmente ofrece siete programas educativos: Ingeniería en Agronomía, Ingeniería Forestal, Ingeniería en Gestión Empresarial, Ingeniería en Administración, Licenciatura en Biología, Ingeniería Informática e Ingeniería en Industrias Alimentarias, así como las Maestrías en: Agroecosistemas Sostenibles (en el PNPC de CONACYT) y Administración.',
                    style: TextStyle(color: Colors.black.withOpacity(0.6)),
                    textAlign: TextAlign.justify,
                  ),
                ),
                ButtonBar(
                  alignment: MainAxisAlignment.end,
                  children: [
                    // ignore: deprecated_member_use
                    FlatButton(
                      textColor: const Color(0xFF6200EE),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => Historia1()),
                        );
                      },
                      child: const Text('VER MAS...'),
                    ),
                    // ignore: deprecated_member_use
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

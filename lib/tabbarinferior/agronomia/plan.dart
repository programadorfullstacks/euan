import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class Agronomia extends StatefulWidget {
  @override
  _AgronomiaPage createState() => _AgronomiaPage();
}

class _AgronomiaPage extends State<Agronomia> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SfPdfViewer.network(
        'https://drive.google.com/uc?export=view&id=16F8OPC2ykEaND_KGtHmdVJtMKCdzHggP',
        key: _pdfViewerKey,
      ),
    );
  }
}

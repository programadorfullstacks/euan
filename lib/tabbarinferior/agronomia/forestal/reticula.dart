import 'package:flutter/material.dart';

import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class Reticulaforestal extends StatefulWidget {
  @override
  _ReticulaforestalPage createState() => _ReticulaforestalPage();
}

class _ReticulaforestalPage extends State<Reticulaforestal> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SfPdfViewer.network(
        '',
        key: _pdfViewerKey,
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:playmedia/tabbarinferior/agronomia/plan.dart';
import 'package:playmedia/tabbarinferior/agronomia/reticula.dart';
import 'package:playmedia/tabbarinferior/agronomia/folleto.dart';

class Forestal extends StatefulWidget {
  @override
  _ForestalState createState() => _ForestalState();
}

class _ForestalState extends State<Forestal>
    with SingleTickerProviderStateMixin {
  String title = 'TopTabBar';
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
        centerTitle: true,
        bottom: TabBar(
          controller: _tabController,
          indicatorColor: Colors.white,
          unselectedLabelColor: Colors.white,
          labelColor: Colors.white,
          indicatorWeight: 5.0,
          indicatorSize: TabBarIndicatorSize.tab,
          tabs: <Widget>[
            Tab(
              text: 'Reticula',
            ),
            Tab(
              text: 'plan de\ndesarrollo',
            ),
            Tab(
              text: 'mas',
            ),
          ],
        ),
      ),
      body: TabBarView(
        controller: _tabController,
        // physics: NeverScrollableScrollPhysics(),
        children: <Widget>[
          Reticula(),
          Agronomia(),
          PaidPage(),
          //Center(child: Text('New')),
          //Text('Draf'),
          //Text('paid')
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

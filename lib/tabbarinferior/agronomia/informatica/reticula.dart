import 'package:flutter/material.dart';

import 'package:syncfusion_flutter_pdfviewer/pdfviewer.dart';

class Reticula extends StatefulWidget {
  @override
  _ReticulaPage createState() => _ReticulaPage();
}

class _ReticulaPage extends State<Reticula> {
  final GlobalKey<SfPdfViewerState> _pdfViewerKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SfPdfViewer.network(
        'https://drive.google.com/uc?export=view&id=1UUsG66KRk038tYlQrMPvAGNcUCxJgJMA',
        key: _pdfViewerKey,
      ),
    );
  }
}
